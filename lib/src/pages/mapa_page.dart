import 'package:flutter/material.dart';

import 'package:flutter_map/plugin_api.dart';
import 'package:latlong/latlong.dart';
import 'package:qrreaderapp/src/models/scan_model.dart';

class MapaPage extends StatefulWidget {
  
  @override
  _MapaPageState createState() => _MapaPageState();
}

class _MapaPageState extends State<MapaPage> {
  final MapController map = new MapController();

  String tipoMapa = 'streets';

  @override
  Widget build(BuildContext context) {

    final ScanModel scan = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text('Coordenadas QR'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.my_location),
            onPressed: () =>  map.move(scan.getLatLng(), 17),
          )
        ],
      ),
      body: _crearFlutterMap( scan ),
      floatingActionButton: _crearBotonFlotante( context ),
    );
  }

  Widget _crearBotonFlotante( BuildContext context ) {

    return FloatingActionButton(
      backgroundColor: Theme.of(context).primaryColor,
      child: Icon(Icons.repeat),
      onPressed: () {

        // streets, dark, light, outdoors, satellite
          if( tipoMapa == 'streets'){
              print('ok');
             tipoMapa = 'dark';
          } else if( tipoMapa == 'dark' ) {
              tipoMapa = 'light';
          } else if( tipoMapa == 'light' ) {
            tipoMapa = 'satellite';
          } else if( tipoMapa == 'satellite' ){
            tipoMapa = 'streets';
          }

        setState(() {
          
        });

      },
    );

  }

  Widget _crearFlutterMap( ScanModel scan ) {
    
    return FlutterMap(
      mapController: map,
      options: MapOptions(
        center: scan.getLatLng(),
        zoom: 17
      ),
      layers: [
        _crearMapa(),
        _crearMarcadores( scan )
      ],
    );

  } 

  TileLayerOptions _crearMapa() {

    return TileLayerOptions(
      
      urlTemplate: 'https://api.mapbox.com/v4/'
        '{id}/{z}/{x}/{y}@2x.png?access_token={accessToken}',
      additionalOptions: {
        'accessToken' : 'pk.eyJ1IjoiamJsYW5kb24iLCJhIjoiY2s0enZneG1sMGZtOTNncWh5eHlrOTJ6dyJ9.Rwf6AcKGbHR5zTw5gfgCqA',
        'id'          : 'mapbox.$tipoMapa' 
                          // streets, dark, light, outdoors, satellite
      }
    );

  }

  MarkerLayerOptions _crearMarcadores(ScanModel scan) {

    return MarkerLayerOptions(
      markers: <Marker>[
        Marker(
          width: 150.0,
          height: 150.0,
          point: scan.getLatLng(),
          builder: ( context ) => Container(
            child: Icon(Icons.location_on,
              size: 70.0,
              color: Theme.of(context).primaryColor),
          )
        )
      ]
    );

  }
}