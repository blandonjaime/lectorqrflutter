import 'dart:io';

import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:qrreaderapp/src/bloc/scans_bloc.dart';
import 'package:qrreaderapp/src/models/scan_model.dart';

import 'package:qrreaderapp/src/pages/direcciones_page.dart';
import 'package:qrreaderapp/src/pages/mapa_page.dart';
import 'package:qrreaderapp/src/pages/mapas_page.dart';
import 'package:qrreaderapp/src/providers/db_provider.dart';
import 'package:qrreaderapp/src/utils/utils.dart' as utils;

class HomePage extends StatefulWidget {
  

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final ScansBloc scanBloc = new ScansBloc();

  int currentIndex = 0;
  String titulo = 'QR Scanner';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(titulo),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.delete_forever),
            onPressed: (){
              scanBloc.borrarScanTodos();
            },
          )
        ],
      ),
      body: _callPage( currentIndex ),
      bottomNavigationBar: _crearBottomNavigationBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.filter_center_focus),
        onPressed: ()=>_scanQR(context),
        backgroundColor: Theme.of(context).primaryColor,
      )
    );
  }

  Widget _callPage(int paginaActual) {

    switch (paginaActual) {
      case 0: return MapasPage();
      case 1: return DireccionesPage();
      default: return MapasPage();
    }

  }

  Widget _crearBottomNavigationBar() {

    return BottomNavigationBar(
      currentIndex: currentIndex,
      onTap: (index) {
        setState( () => currentIndex = index );
      },
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.map),
          title: Text('Mapas')
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.brightness_5),
          title: Text('Direcciones')
        ),
      ],
    );

  }

  _scanQR(BuildContext context) async {
  
    String futureString;

    try {
       futureString = await BarcodeScanner.scan();
    } catch (e) {
       futureString = e.toString();
    }

    if ( futureString != null ) {
      
      final nuevoScan = ScanModel(valor: futureString);

      scanBloc.agregarScan(nuevoScan);

      if ( Platform.isIOS ) {

        Future.delayed(Duration(milliseconds: 750), () {
          utils.abrirScan(context,nuevoScan);
        });

      }else { 
        utils.abrirScan(context,nuevoScan);
      }

      
      //await DBProvider.db.nuevoScan(nuevoScan);

    }

    // try {
    //   futureString = await BarcodeScanner.scan();
    // } catch (e) {
    //   futureString = e.toString();
    // }

    // print('Future String: $futureString');

    // if( futureString != null ) {
    //    setState( () {
    //      titulo = futureString;
    //    });
    //   print('Tenemos Info');
    // }

  }
}