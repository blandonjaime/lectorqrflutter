import 'package:flutter/material.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:qrreaderapp/src/models/scan_model.dart';

abrirScan(BuildContext context, ScanModel scan ) async {

  if (scan.tipo == 'http') {

    final url = scan.valor ;

    print( url );

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'No se puede abrir: $url';
    }

  }else{
    Navigator.pushNamed(context, 'mapa', arguments: scan);
  }
}